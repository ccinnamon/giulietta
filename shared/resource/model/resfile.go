// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package model

import (
	rice "github.com/GeertJohan/go.rice"

	"regexp"
	"strings"
)

//ResourceFile represents an resource file.
type ResourceFile struct {
	mediatype string
	size      int
	name      string
	path      string
	box       rice.Box
	file      *rice.File
}

//ResourceFileNew creates a new resource file, given a box and a name
func ResourceFileNew(box rice.Box, path string, name string) (*ResourceFile, error) {
	regex := regexp.MustCompile("[^\\\\/]+")
	div := regex.FindAllString(path, -1)
	if div == nil {
		div = make([]string, 1)
	}
	div = append(div, name)
	completePath := strings.Join(div, "/")
	ricefile, err := box.Open(name)
	if err == nil {
		return &ResourceFile{"", -1, name, completePath, box, ricefile}, nil
	} else {
		return nil, err
	}
}

//MediaType returns the file media type
func (res ResourceFile) MediaType() string {
	return res.name
}

//Size returns the file size
func (res ResourceFile) Size() uint {
	return uint(res.size)
}

//Name returns the file name
func (res ResourceFile) Name() string {
	return res.name
}

//Path returns the file path within the box
func (res ResourceFile) Path() string {
	return res.path
}

//Box returns the parent box
func (res ResourceFile) Box() rice.Box {
	return res.box
}

//File returns an boxed file, stream.
func (res ResourceFile) File() rice.File {
	return *res.file
}

//Close the open resource file.
func (res ResourceFile) Close() error {
	return res.file.Close()
}
