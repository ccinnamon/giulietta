// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package model

import (
	"errors"

	tag "bitbucket.org/ccinnamon/giulietta/shared/tag/model"
	"github.com/satori/go.uuid"
)

//Folder represents a folder of the editor.
type Folder struct {
	name  string
	State FileType
	id    uuid.UUID
	owner string
	files []File
	tags  []tag.Tag
}

//FolderNew creates a new folder, of the given type.
func FolderNew(name string, state FileType) (*Folder, error) {
	if name == "" {
		return nil, errors.New("Folder must have a name")
	}
	return &Folder{name, state, uuid.NewV1(), "nobody", make([]File, 0, 2), make([]tag.Tag, 0, 2)}, nil
}

//Name returns the name of the folder.
func (f *Folder) Name() string {
	return f.name
}

//SetName allows to change the name of a folder.
func (f *Folder) SetName(name string) error {
	if name == "" {
		return errors.New("Folder name must not be empty")
	}
	f.name = name
	return nil
}

//Toggle changes the state of a folder.
func (f *Folder) Toggle() {
	if f.State == VIRTUAL {
		f.State = PHYSICAL
	} else {
		f.State = VIRTUAL
	}
}

//ID returns the internal uuid of this folder.
func (f *Folder) ID() uuid.UUID {
	return f.id
}

//Owner returns the owner of this folder.
func (f *Folder) Owner() string {
	return f.owner
}

//Files returns a list of files contained in this folder.
func (f *Folder) Files() []File {
	return f.files
}

//FileCount returns the number of files in this directory.
func (f *Folder) FileCount() uint {
	return uint(len(f.files))
}

//File returns a given file.
func (f *Folder) File(index uint) (*File, error) {
	if len(f.files) < (int(index) - 1) {
		return nil, errors.New("There is no file with the given index")
	}
	return &f.files[index], nil
}

//AddFile adds a new file to this folder.
func (f *Folder) AddFile(file *File) {
	f.files = append(f.files, *file)
}

//RemoveFile removes a file from this folder.
func (f *Folder) RemoveFile(position uint) {
	f.files = append(f.files[:position], f.files[position+1:]...)
}

//Tags returns a list of tags of this folder.
func (f *Folder) Tags() []tag.Tag {
	return f.tags
}

//TagCount returns the number of tags of this folder.
func (f *Folder) TagCount() uint {
	return uint(len(f.tags))
}

//Tag returns a tag from this folder.
func (f *Folder) Tag(index uint) (*tag.Tag, error) {
	if len(f.tags) < (int(index) - 1) {
		return nil, errors.New("There is no tag with the given index")
	}
	return &f.tags[index], nil
}

//AddTag adds a new tag to this folder.
func (f *Folder) AddTag(tag *tag.Tag) {
	f.tags = append(f.tags, *tag)
}

//RemoveTag removes a tag from this folder.
func (f *Folder) RemoveTag(position uint) {
	f.tags = append(f.tags[:position], f.tags[position+1:]...)
}
