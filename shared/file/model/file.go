// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package model

import (
	"errors"
	"os"

	model "bitbucket.org/ccinnamon/giulietta/shared/tag/model"
	"github.com/satori/go.uuid"
)

//FileStatus contains the status of a file.
type FileStatus uint

const (
	//PRISTINE is a filestatus value that specifies that the file has been created/opened and isn't modified.
	PRISTINE FileStatus = iota
	//MODIFIED is a filestatus value that specifies that the file has been edited inside the editor.
	MODIFIED
	//EDITING is a filestatus value that specifies that the file is being edited in the editor (but changed have not been saved).
	EDITING
)

//FileType The type of file.
type FileType uint

const (
	//VIRTUAL specifies that the file only exists in the editor.
	VIRTUAL FileType = iota
	//PHYSICAL specifies a file that exists on a physical medium like a disk.
	PHYSICAL
)

//File represents a editor file.
type File struct {
	name               string
	ftype              string
	file               *os.File
	id                 uuid.UUID
	size               uint
	owner              string
	ModificationStatus FileStatus
	state              FileType
	filename           string
	filters            []func(file *File) error
	tags               []model.Tag
}

//FileNew creates a new file, with the given name.
func FileNew(name string) (*File, error) {
	if name == "" {
		return nil, errors.New("File name should not be empty")
	}
	return &File{name, "BINARY", nil, uuid.NewV1(), 0, "nobody", PRISTINE, VIRTUAL, "", make([]func(file *File) error, 0, 2),
		make([]model.Tag, 0, 2)}, nil
}

//Name represents a file name.
func (f *File) Name() string {
	return f.name
}

//SetName is a function that allows one to change the file name.
func (f *File) SetName(name string) error {
	if name == "" {
		return errors.New("File name should not be empty")
	}
	f.name = name
	return nil
}

//Type represents the media-type of the file or BINARY for an unknown binary file.
func (f *File) Type() string {
	return f.ftype
}

//FileP returns an pointer to the file stream.
func (f *File) FileP() *os.File {
	return f.file
}

//ID returns the identifier of the file (an unique UUID code in the editor).
func (f *File) ID() uuid.UUID {
	return f.id
}

//Size returns the size of the file in bytes.
func (f *File) Size() uint {
	return f.size
}

//Owner returns the owner of the file as specified by the os.
func (f *File) Owner() string {
	return f.owner
}

//State shows the state of the file (virtual or physical).
func (f *File) State() FileType {
	return f.state
}

//SetState allows one to change the state of the file.
func (f *File) SetState(ftype FileType, filename string) error {
	if ftype == PHYSICAL && filename == "" {
		return errors.New("Filename must not be empty in a physical file")
	}
	f.filename = filename
	f.state = ftype
	return nil
}

//Tags returns a list of tags applied to this file.
func (f *File) Tags() []model.Tag {
	return f.tags
}

//TagCount is the number of tags that this file contains.
func (f *File) TagCount() uint {
	return uint(len(f.tags))
}

//Tag returns the tag in the given index.
func (f *File) Tag(index uint) (*model.Tag, error) {
	if len(f.tags) < (int(index) - 1) {
		return nil, errors.New("There is no tag with the given index")
	}
	return &f.tags[index], nil
}

//AddTag adds a tag to this file.
func (f *File) AddTag(tag *model.Tag) {
	f.tags = append(f.tags, *tag)
}

//RemoveTag removes a tag from this file
func (f *File) RemoveTag(position uint) {
	f.tags = append(f.tags[:position], f.tags[position+1:]...)
}

//TextFile represents a text file.
type TextFile struct {
	File
	encoding   string
	lineNumber uint
}

//TextFileNew creates a new text file with the given name and encoding.
func TextFileNew(name string, encoding string) (*TextFile, error) {
	if name == "" || encoding == "" {
		return nil, errors.New("File name and encoding should not be empty")
	}
	return &TextFile{File{name, "TEXT", nil, uuid.NewV1(), 0, "UNKNOWN", PRISTINE, VIRTUAL, "", make([]func(file *File) error, 0, 2),
		make([]model.Tag, 0, 2)}, encoding, 0}, nil
}

//Encoding returns the encoding of the text file.
func (f *TextFile) Encoding() string {
	return f.encoding
}

//LineNumber returns the number of lines of the text file.
func (f *TextFile) LineNumber() uint {
	return f.lineNumber
}
