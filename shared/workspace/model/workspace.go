// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package model

import (
	"errors"

	. "bitbucket.org/ccinnamon/giulietta/shared/project/model"
	. "bitbucket.org/ccinnamon/giulietta/shared/tag/model"
	"github.com/satori/go.uuid"
)

type Workspace struct {
	name         string
	physicalPath string
	projects     []Project
	id           uuid.UUID
	tags         []Tag
}

func WorkspaceNew(name string, physicalPath string) (*Workspace, error) {
	if name == "" || physicalPath == "" {
		return nil, errors.New("Name or PhysicalPath is empty")
	}

	return &Workspace{name, physicalPath, make([]Project, 0, 2), uuid.NewV1(),
		make([]Tag, 0, 2)}, nil
}

func (w *Workspace) Name() string {
	return w.name
}

func (w *Workspace) SetName(name string) error {
	if name == "" {
		return errors.New("Name is empty")
	}
	w.name = name
	return nil
}

func (w *Workspace) PhysicalFolder() string {
	return w.physicalPath
}

func (w *Workspace) SetPhysicalFolder(physicalPath string) error {
	if physicalPath == "" {
		return errors.New("PhysicalPath is empty")
	}
	w.physicalPath = physicalPath
	return nil
}

func (w *Workspace) ProjectCount() uint {
	return uint(len(w.projects))
}

func (w *Workspace) Project(index uint) (*Project, error) {
	if len(w.projects) < (int(index) - 1) {
		return nil, errors.New("There is no project with the given index")
	}
	return &w.projects[index], nil
}

func (w *Workspace) AddProject(project *Project) {
	w.projects = append(w.projects, *project)
}

func (w *Workspace) RemoveProject(position uint) {
	w.projects = append(w.projects[:position], w.projects[position+1:]...)
}

func (w *Workspace) ID() uuid.UUID {
	return w.id
}

func (w *Workspace) Tags() []Tag {
	return w.tags
}

func (w *Workspace) TagCount() uint {
	return uint(len(w.tags))
}

func (w *Workspace) Tag(index uint) (*Tag, error) {
	if len(w.tags) < (int(index) - 1) {
		return nil, errors.New("There is no tag with the given index")
	}
	return &w.tags[index], nil
}

func (w *Workspace) AddTag(tag *Tag) {
	w.tags = append(w.tags, *tag)
}

func (w *Workspace) RemoveTag(position uint) {
	w.tags = append(w.tags[:position], w.tags[position+1:]...)
}
