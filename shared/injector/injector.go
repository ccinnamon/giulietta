// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package injector

import (
	"errors"
	"reflect"

	list "github.com/emirpasic/gods/lists/doublylinkedlist"
	redblack "github.com/emirpasic/gods/trees/redblacktree"
)

type Injector struct {
	entities   *list.List
	interfaces *redblack.Tree
	names      *redblack.Tree
}

var injector *Injector = injectorNew()

func injectorNew() *Injector {
	return &Injector{list.New(), redblack.NewWithStringComparator(), redblack.NewWithStringComparator()}
}

func (injector Injector) AddInterface(iface reflect.Type, obj reflect.Type) error {
	_, rawentity := injector.entities.Find(func(_ int, value interface{}) bool {
		if (value.(*Information)).Intface() == iface {
			return true
		} else {
			return false
		}
	})
	entity, ok := rawentity.(*Information)
	if entity == nil || ok != false {
		entity, _ = informationNewWithInterface(iface, obj)
		injector.entities.Add(&entity)
	} else {
		return errors.New("Object already exists")
	}
	val, ok := injector.interfaces.Get(iface.String())
	if ok {
		injector.interfaces.Remove(iface.String())
	} else {
		val = list.New()
	}
	(val.(*list.List)).Add(&entity)
	injector.interfaces.Put(iface.String(), (val.(*list.List)))
	return nil
}

func (injector Injector) AddNamed(name string, obj reflect.Type) error {
	_, rawentity := injector.entities.Find(func(_ int, value interface{}) bool {
		if (value.(*Information)).Name() == name {
			return true
		} else {
			return false
		}
	})
	entity, ok := rawentity.(*Information)
	if entity == nil || ok != false {
		entity, _ = informationNewWithName(name, obj)
		injector.entities.Add(&entity)
	} else {
		return errors.New("Object already exists")
	}
	val, ok := injector.names.Get(name)
	if ok {
		injector.names.Remove(name)
	} else {
		val = list.New()
	}
	(val.(*list.List)).Add(&entity)
	injector.names.Put(name, (val.(*list.List)))
	return nil
}

func (injector Injector) AddNamedInterface(name string, iface reflect.Type, obj reflect.Type) error {
	_, rawentity := injector.entities.Find(func(_ int, value interface{}) bool {
		if (value.(*Information)).Intface() == iface {
			return true
		} else {
			return false
		}
	})
	entity, ok := rawentity.(*Information)
	if entity == nil || ok != false {
		entity, _ = informationNewWithInterface(iface, obj)
		injector.entities.Add(&entity)
	} else {
		return errors.New("Object already exists")
	}
	val, ok := injector.interfaces.Get(iface.String())
	if ok {
		injector.interfaces.Remove(iface.String())
	} else {
		val = list.New()
	}
	(val.(*list.List)).Add(&entity)
	injector.interfaces.Put(iface.String(), (val.(*list.List)))
	val, ok = injector.names.Get(name)
	if ok {
		injector.names.Remove(name)
	} else {
		val = list.New()
	}
	(val.(*list.List)).Add(&entity)
	injector.names.Put(name, (val.(*list.List)))
	return nil
}

func (injector Injector) GetByInterface(iface reflect.Type) (interface{}, error) {
	rawvalue, ok := injector.interfaces.Get(iface.String())
	value, convertok := rawvalue.(*Information)
	if ok && convertok {
		return value.createObj()
	} else {
		return nil, errors.New("Interface not registered")
	}
}

func (injector Injector) GetByNamed(name string) (interface{}, error) {
	rawvalue, ok := injector.names.Get(name)
	value, convertok := rawvalue.(*Information)
	if ok && convertok {
		return value.createObj()
	} else {
		return nil, errors.New("Name not registered")
	}
}

func (injector Injector) GetByNamedInterface(name string, iface reflect.Type) (interface{}, error) {
	_, rawvalue := injector.entities.Find(func(_ int, value interface{}) bool {
		if (value.(*Information)).Intface() == iface && (value.(*Information)).Name() == name {
			return true
		} else {
			return false
		}
	})
	value, convertok := rawvalue.(*Information)
	if convertok {
		return value.createObj()
	} else {
		return nil, errors.New("Interface and name not registered")
	}
}

func (injector Injector) RemoveInterface(iface reflect.Type) {
	injector.interfaces.Remove(iface)
}

func (injector Injector) RemoveNamed(name string) {
	injector.names.Remove(name)
}

func (injector Injector) RemoveNamedInterface(name string, iface reflect.Type) {
	index, _ := injector.entities.Find(func(_ int, value interface{}) bool {
		if (value.(*Information)).Intface() == iface && (value.(*Information)).Name() == name {
			return true
		} else {
			return false
		}
	})
	injector.entities.Remove(index)
	injector.interfaces.Remove(iface)
	injector.names.Remove(name)
}
