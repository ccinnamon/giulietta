// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package injector

import (
	"reflect"
)

type Information struct {
	name  string
	iface reflect.Type
	obj   reflect.Type
}

func informationNewWithNameInterface(name string, iface reflect.Type, obj reflect.Type) (*Information, error) {
	return &Information{name, iface, obj}, nil
}

func informationNewWithName(name string, obj reflect.Type) (*Information, error) {
	return &Information{name, nil, obj}, nil
}

func informationNewWithInterface(iface reflect.Type, obj reflect.Type) (*Information, error) {
	return &Information{"", iface, obj}, nil
}

func (information Information) createObj() (interface{}, error) {
	return nil, nil
}

func (information Information) Name() string {
	return information.name
}

func (information Information) Intface() reflect.Type {
	return information.iface
}

func (information Information) Obj() reflect.Type {
	return information.obj
}
