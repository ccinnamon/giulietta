// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package model

import (
	"errors"
)

type Tag struct {
	name  string
	Value string
}

func TagNew(name string, value string) (*Tag, error) {
	if name == "" {
		return nil, errors.New("Name cannot be empty")
	}
	return &Tag{name, value}, nil
}

func (t *Tag) Name() string {
	return t.name
}
