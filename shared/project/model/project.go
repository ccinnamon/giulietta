// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

//Package model contains the project model
package model

import (
	"errors"
	"time"

	file "bitbucket.org/ccinnamon/giulietta/shared/file/model"
	tag "bitbucket.org/ccinnamon/giulietta/shared/tag/model"
	"github.com/satori/go.uuid"
)

//ProjectState represents a state of a project.
type ProjectState int

const (
	ACTIVE ProjectState = iota
	INACTIVE
)

type ProjectPermissions int

const (
	READABLE ProjectPermissions = iota
	EDITABLE
)

type Project struct {
	name                string
	id                  uuid.UUID
	State               ProjectState
	Permission          ProjectPermissions
	Homepage            string
	Maintainer          string
	ProgrammingLanguage string
	License             string
	Description         string
	Created             time.Time
	BugTracker          string
	testers             []string
	screenshots         []string
	translators         []string
	documenters         []string
	platforms           []string
	mailingLists        []string
	developers          []string
	repositories        []Repository
	folders             []file.Folder
	files               []file.File
	tags                []tag.Tag
}

func ProjectNew(name string, state ProjectState, permission ProjectPermissions) (*Project, error) {
	if name == "" {
		return nil, errors.New("Project name is empty")
	}
	return &Project{name, uuid.NewV1(), state, permission, "", "", "",
		"", "", time.Now(), "", make([]string, 0, 2),
		make([]string, 0, 2), make([]string, 0, 2),
		make([]string, 0, 2), make([]string, 0, 2),
		make([]string, 0, 2), make([]string, 0, 2),
		make([]Repository, 0, 2), make([]file.Folder, 0, 2),
		make([]file.File, 0, 2), make([]tag.Tag, 0, 2)}, nil
}

func (p *Project) Name() string {
	return p.name
}

func (p *Project) SetName(name string) error {
	if name == "" {
		return errors.New("Project name is empty")
	}
	p.name = name
	return nil
}

func (p *Project) ID() uuid.UUID {
	return p.id
}

func (p *Project) Toggle() {
	if p.State == ACTIVE {
		p.State = INACTIVE
	} else {
		p.State = ACTIVE
	}
}

func (p *Project) PermissionToggle() {
	if p.Permission == READABLE {
		p.Permission = EDITABLE
	} else {
		p.Permission = READABLE
	}
}

func (p *Project) Testers() []string {
	return p.testers
}

func (p *Project) TesterCount() uint {
	return uint(len(p.testers))
}

func (p *Project) Tester(index uint) (string, error) {
	if len(p.testers) < (int(index) - 1) {
		return "", errors.New("There is no tester with the given index")
	}
	return p.testers[index], nil
}

func (p *Project) AddTester(tester string) {
	p.testers = append(p.testers, tester)
}

func (p *Project) RemoveTester(position uint) {
	p.testers = append(p.testers[:position], p.testers[position+1:]...)
}

func (p *Project) Screenshots() []string {
	return p.screenshots
}

func (p *Project) ScreenshotCount() uint {
	return uint(len(p.screenshots))
}

func (p *Project) Screenshot(index uint) (string, error) {
	if len(p.screenshots) < (int(index) - 1) {
		return "", errors.New("There is no screenshot with the given index")
	}
	return p.screenshots[index], nil
}

func (p *Project) AddScreenshot(screenshot string) {
	p.screenshots = append(p.screenshots, screenshot)
}

func (p *Project) RemoveScreenshot(position uint) {
	p.screenshots = append(p.screenshots[:position], p.screenshots[position+1:]...)
}

func (p *Project) Translators() []string {
	return p.translators
}

func (p *Project) TranslatorCount() uint {
	return uint(len(p.translators))
}

func (p *Project) Translator(index uint) (string, error) {
	if len(p.translators) < (int(index) - 1) {
		return "", errors.New("There is no translator with the given index")
	}
	return p.translators[index], nil
}

func (p *Project) AddTranslator(translator string) {
	p.translators = append(p.translators, translator)
}

func (p *Project) RemoveTranslator(position uint) {
	p.translators = append(p.translators[:position], p.translators[position+1:]...)
}

func (p *Project) Documenters() []string {
	return p.documenters
}

func (p *Project) DocumenterCount() uint {
	return uint(len(p.documenters))
}

func (p *Project) Documenter(index uint) (string, error) {
	if len(p.documenters) < (int(index) - 1) {
		return "", errors.New("There is no documenter with the given index")
	}
	return p.documenters[index], nil
}

func (p *Project) AddDocumenter(documenter string) {
	p.documenters = append(p.documenters, documenter)
}

func (p *Project) RemoveDocumenter(position uint) {
	p.documenters = append(p.documenters[:position], p.documenters[position+1:]...)
}

func (p *Project) Platforms() []string {
	return p.platforms
}

func (p *Project) PlatformCount() uint {
	return uint(len(p.platforms))
}

func (p *Project) Platform(index uint) (string, error) {
	if len(p.platforms) < (int(index) - 1) {
		return "", errors.New("There is no platform with the given index")
	}
	return p.platforms[index], nil
}

func (p *Project) AddPlatform(platform string) {
	p.platforms = append(p.platforms, platform)
}

func (p *Project) RemovePlatform(position uint) {
	p.platforms = append(p.platforms[:position], p.platforms[position+1:]...)
}

func (p *Project) MailingLists() []string {
	return p.mailingLists
}

func (p *Project) MailingListCount() uint {
	return uint(len(p.mailingLists))
}

func (p *Project) MailingList(index uint) (string, error) {
	if len(p.mailingLists) < (int(index) - 1) {
		return "", errors.New("There is no mailing list with the given index")
	}
	return p.mailingLists[index], nil
}

func (p *Project) AddMailingList(mailingList string) {
	p.mailingLists = append(p.mailingLists, mailingList)
}

func (p *Project) RemoveMailingList(position uint) {
	p.mailingLists = append(p.mailingLists[:position], p.mailingLists[position+1:]...)
}

func (p *Project) Developers() []string {
	return p.developers
}

func (p *Project) DeveloperCount() uint {
	return uint(len(p.developers))
}

func (p *Project) Developer(index uint) (string, error) {
	if len(p.developers) < (int(index) - 1) {
		return "", errors.New("There is no developer with the given index")
	}
	return p.developers[index], nil
}

func (p *Project) AddDeveloper(developer string) {
	p.developers = append(p.developers, developer)
}

func (p *Project) RemoveDeveloper(position uint) {
	p.developers = append(p.developers[:position], p.developers[position+1:]...)
}

func (p *Project) Repositories() []Repository {
	return p.repositories
}

func (p *Project) RepositoryCount() uint {
	return uint(len(p.repositories))
}

func (p *Project) Repository(index uint) (*Repository, error) {
	if len(p.repositories) < (int(index) - 1) {
		return nil, errors.New("There is no repository with the given index")
	}
	return &p.repositories[index], nil
}

func (p *Project) AddRepository(repository *Repository) {
	p.repositories = append(p.repositories, *repository)
}

func (p *Project) RemoveRepository(position uint) {
	p.repositories = append(p.repositories[:position], p.repositories[position+1:]...)
}

func (p *Project) Folders() []file.Folder {
	return p.folders
}

func (p *Project) FolderCount() uint {
	return uint(len(p.folders))
}

func (p *Project) Folder(index uint) (*file.Folder, error) {
	if len(p.folders) < (int(index) - 1) {
		return nil, errors.New("There is no folder with the given index")
	}
	return &p.folders[index], nil
}

func (p *Project) AddFolder(folder *file.Folder) {
	p.folders = append(p.folders, *folder)
}

func (p *Project) RemoveFolder(position uint) {
	p.folders = append(p.folders[:position], p.folders[position+1:]...)
}

func (p *Project) Files() []file.File {
	return p.files
}

func (p *Project) FileCount() uint {
	return uint(len(p.files))
}

func (p *Project) File(index uint) (*file.File, error) {
	if len(p.files) < (int(index) - 1) {
		return nil, errors.New("There is no file with the given index")
	}
	return &p.files[index], nil
}

func (p *Project) AddFile(file *file.File) {
	p.files = append(p.files, *file)
}

func (p *Project) RemoveFile(position uint) {
	p.files = append(p.files[:position], p.files[position+1:]...)
}

func (p *Project) Tags() []tag.Tag {
	return p.tags
}

func (p *Project) TagCount() uint {
	return uint(len(p.tags))
}

func (p *Project) Tag(index uint) (*tag.Tag, error) {
	if len(p.tags) < (int(index) - 1) {
		return nil, errors.New("There is no tag with the given index")
	}
	return &p.tags[index], nil
}

func (p *Project) AddTag(tag *tag.Tag) {
	p.tags = append(p.tags, *tag)
}

func (p *Project) RemoveTag(position uint) {
	p.tags = append(p.tags[:position], p.tags[position+1:]...)
}

type RepositoryType int

const (
	UNKNOWN RepositoryType = iota
	GIT
	SVN
	CVS
	HG
	OTHER
)

type Repository struct {
	URL  string
	Type RepositoryType
}

func RepositoryNew(url string, repotype RepositoryType) (*Repository, error) {
	return &Repository{url, repotype}, nil
}
