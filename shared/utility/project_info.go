// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

package utility

type VersionInfo struct {
	Major uint32
	Minor uint32
	Patch uint32
	Build uint64
}

type ProjectInfo struct {
	Name        string
	Version     VersionInfo
	Description string
	License     string
	Developer   string
}

var GiuliettaInfo ProjectInfo = ProjectInfo{
	"Giulietta", VersionInfo{0, 1, 4, 1},
	"Giulietta is a text editor written using QT5 and Go",
	"GPLv3", "carddamom <carddamom at outlook dot pt>"}
