// Copyright (C) 2016-2017 carddamom
// This file is part of giulietta.
//
// Guilietta is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guilietta is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guilietta.  If not, see <http://www.gnu.org/licenses/>.

//Package main contains the giulietta ui editor
package main

import (
	//"bitbucket.org/ccinnamon/giulietta/cmd/giulietta/view"
	"github.com/ian-kent/go-log/levels"
	"github.com/ian-kent/go-log/log"
	"github.com/ttacon/chalk"
)

func main() {
	logger := log.Logger("main")
	logger.SetLevel(levels.INFO)
	logger.Info(chalk.Bold.TextStyle(chalk.Blue.Color("My first logging")))
}
