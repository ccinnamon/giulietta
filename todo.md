# TODO

This is a list of ideas that should be done in giulietta, to get the editor I want...

## ROADMAP

Giulietta is going to have (at first), the following releases:

+ 0.5.0
+ 1.0.0
+ 2.0.0
+ 3.0.0
+ 4.0.0
+ ...
+ Profit!

So, in the releases, the following features are going to be developed.

### 0.5.0

+ Have a repository in a public place;
+ Create a basic project in python that can be build in a repeateable and automatized way, to produce executables or artifacs;
+ Get a BDD based test framework in place;
+ Place it in a continuous integration platform, in order to run the tests, at each commit;
+ Place it showing an basic editor UI;
+ Be able to do basic editing, like undo, redo, copy, paste, cut, delete, find, replace and edit text;
+ Be able to create, open and save workspaces, projects, folders and files;
+ Be able to open multiple files, in multiple tabs;

### 1.0.0

+ Add support for project and file plugins;
+ Add support for syntax highlighting;
+ Add support for line numbering;
+ Add support for formatting plugins;
+ Add support for priting;
+ Add support for help plugins;

### 2.0.0

+ ...
