<?xml version="1.01" encoding="UTF-8"?>
<!DOCTYPE language SYSTEM "language.dtd">
<language name="LaTeX" version="1.12" section="Markup" kateversion="2.3" extensions="*.tex; *.ltx; *.dtx; *.sty; *.cls;" mimetype="text/x-tex" casesensitive="1" author="Jeroen Wijnhout (Jeroen.Wijnhout@kdemail.net)" license="LGPL" >
  <highlighting>
    <contexts>
      <context name="Normal Text" attribute="Normal Text" lineEndContext="#stay">
        <RegExpr String="\\begin(?=[^a-zA-Z])" attribute="Structure" context="FindEnvironment" beginRegion="block" />
        <RegExpr String="\\end(?=[^a-zA-Z])" attribute="Structure" context="FindEnvironment" endRegion="block" />
        <RegExpr String="\\(label|pageref|ref|cite)(?=[^a-zA-Z])" attribute="Structure" context="Label"/>
        <RegExpr String="\\(part|chapter|section|subsection|subsubsection|paragraph|subparagraph)(?=[^a-zA-Z])" attribute="Structure" context="#stay"/>
        <StringDetect String="\renewcommand" attribute="Keyword" context="ToEndOfLine"/>
        <StringDetect String="\newcommand" attribute="Keyword" context="ToEndOfLine"/>
        <StringDetect String="\(" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <StringDetect String="\[" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <DetectChar char="\" attribute="Keyword" context="ContrSeq"/>

        <StringDetect String="$$" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <DetectChar char="$" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <RegExpr String="^\s*%\s*BEGIN.*$" attribute="Region Marker" context="#stay" beginRegion="regionMarker" />
        <RegExpr String="^\s*%\s*END.*$" attribute="Region Marker" context="#stay" endRegion="regionMarker" />
        <DetectChar char="%" attribute="Comment" context="Comment"/>
      </context>
      <context name="ContrSeq" attribute="Keyword" lineEndContext="#pop">
        <StringDetect String="verb*" attribute="Keyword" context="Verb"/>
        <StringDetect String="verb" attribute="Keyword" context="Verb"/>
        <RegExpr String="[a-zA-Z]+" attribute="Keyword" context="#pop"/>
        <RegExpr String="[^a-zA-Z]" attribute="Keyword" context="#pop" />
      </context>
      <context name="ToEndOfLine" attribute="Normal Text" lineEndContext="#pop">
      </context>

      <context name="FindEnvironment" attribute="Normal Text" lineEndContext="#stay">
        <DetectChar char="{" attribute="Normal Text" context="Environment"/>
        <RegExpr String="[^\s]" attribute="Normal Text" context="#pop"/>
      </context>
      <context name="Environment" attribute="Environment" lineEndContext="#stay">
        <DetectChar char="}" attribute="Normal Text" context="#pop#pop"/>
        <DetectChar char="]" attribute="Normal Text" context="#pop#pop"/>
        <RegExpr String="(verbatim|lstlisting|boxedverbatim|Verbatim)\*?" attribute="Environment" context="VerbatimEnv"/>
        <RegExpr String="(equation|displaymath|eqnarray|math|multline)\*?" attribute="Environment" context="MathEnv"/>
      </context>

      <context name="Label" attribute="Normal Text" lineEndContext="#pop#pop" fallthrough="true" fallthroughContext="#pop#pop">
        <RegExpr String="\s*\{\s*" attribute="Normal Text" context="#stay"/>
        <RegExpr String="[^\}\{]+" attribute="Environment" context="#stay"/>
        <RegExpr String="\s*\}\s*" attribute="Normal Text" context="#pop"/>
      </context>

      <context name="Verb" attribute="Verbatim" lineEndContext="#pop" fallthrough="true" fallthroughContext="#pop">
        <RegExpr String="(.).*\1" attribute="Verbatim" context="#pop#pop" minimal="true"/>
      </context>
      <context name="VerbatimEnv" attribute="Environment" lineEndContext="#stay">
        <DetectChar char="*" attribute="Normal Text" context="#stay"/>
        <DetectChar char="}" attribute="Normal Text" context="Verbatim"/>
        <RegExpr String="[^\s]" attribute="Normal Text" context="#pop"/>
      </context>
      <context name="Verbatim" attribute="Verbatim" lineEndContext="#stay">
        <RegExpr String="\\end(?=\{(verbatim|lstlisting|boxedverbatim|Verbatim)\*?\})" attribute="Structure"  context="VerbFindEnd"/>
      </context>
      <context name="VerbFindEnd" attribute="Normal Text" lineEndContext="#pop" fallthrough="true" fallthroughContext="#pop">
        <DetectChar char="{" attribute="Normal Text" context="#stay"/>
        <RegExpr String="(verbatim|lstlisting|boxedverbatim|Verbatim)\*?" attribute="Environment" context="#stay"/>
        <DetectChar char="}" attribute="Normal Text" context="#pop#pop#pop#pop#pop" endRegion="block"/>
      </context>

      <context name="MathMode" attribute="Math" lineEndContext="#stay">
        <Detect2Chars char="\" char1="]" attribute="Math" context="#pop" endRegion="mathMode" />
        <Detect2Chars char="\" char1=")" attribute="Math" context="#pop" endRegion="mathMode" />
        <RegExpr String="\\begin(?=[^a-zA-Z])" attribute="Keyword Mathmode" context="#stay" beginRegion="block" />
        <RegExpr String="\\end(?=[^a-zA-Z])" attribute="Keyword Mathmode" context="#stay" endRegion="block" />
        <DetectChar char="\" attribute="Keyword Mathmode" context="MathContrSeq"/>
        <StringDetect String="$$" attribute="Math" context="#pop" endRegion="mathMode" />
        <DetectChar char="$" attribute="Math" context="#pop" endRegion="mathMode" />
        <RegExpr String="^\s*%\s*BEGIN.*$" attribute="Region Marker" context="#stay" beginRegion="regionMarker" />
        <RegExpr String="^\s*%\s*END.*$" attribute="Region Marker" context="#stay" endRegion="regionMarker" />
        <DetectChar char="%" attribute="Comment" context="Comment"/>
      </context>
      <context name="MathModeEnv" attribute="Math" lineEndContext="#stay">
        <RegExpr String="\\end(?=\s*\{\s*[a-zA-Z]*(equation|displaymath|eqnarray|math|multline)\*?\s*\})" attribute="Structure" context="MathFindEnd"/>
        <RegExpr String="\\begin(?=[^a-zA-Z])" attribute="Keyword Mathmode" context="#stay" beginRegion="block" />
        <RegExpr String="\\end(?=[^a-zA-Z])" attribute="Keyword Mathmode" context="#stay" endRegion="block" />
        <StringDetect String="\(" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <StringDetect String="\[" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <DetectChar char="\" attribute="Keyword Mathmode" context="MathContrSeq"/>
        <StringDetect String="$$" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <DetectChar char="$" attribute="Math" context="MathMode" beginRegion="mathMode" />
        <RegExpr String="^\s*%\s*BEGIN.*$" attribute="Region Marker" context="#stay" beginRegion="regionMarker" />
        <RegExpr String="^\s*%\s*END.*$" attribute="Region Marker" context="#stay" endRegion="regionMarker" />
        <DetectChar char="%" attribute="Comment" context="Comment"/>
      </context>
      <context name="MathEnv" attribute="Environment" lineEndContext="#stay">
        <DetectChar char="}" attribute="Normal Text" context="MathModeEnv"/>
        <RegExpr String="[^\s]" attribute="Normal Text" context="#pop"/>
      </context>
      <context name="MathFindEnd" attribute="Normal Text" lineEndContext="#pop" fallthrough="true" fallthroughContext="#pop">
        <DetectChar char="{" attribute="Normal Text" context="#stay"/>
        <RegExpr String="[a-zA-Z]*(equation|displaymath|eqnarray|math|multline)\*?" attribute="Environment" context="#stay"/>
        <RegExpr String="\s*" attribute="Normal Text" context="#stay"/>
        <DetectChar char="}" attribute="Normal Text" context="#pop#pop#pop#pop#pop"  endRegion="block"/>
      </context>
      <context name="MathContrSeq" attribute="Keyword Mathmode" lineEndContext="#pop">
        <RegExpr String="[a-zA-Z]+" attribute="Keyword Mathmode" context="#pop"/>
        <RegExpr String="[^a-zA-Z]" attribute="Keyword Mathmode" context="#pop" />
      </context>

      <context name="Comment" attribute="Comment" lineEndContext="#pop">
      </context>
    </contexts>
    <itemDatas>
      <itemData name="Normal Text" defStyleNum="dsNormal"/>
      <itemData name="Keyword" defStyleNum="dsNormal" color="#800000" selColor="#60FFFF" bold="0" italic="0"/>
      <itemData name="Comment" defStyleNum="dsComment"/>
      <itemData name="Math" defStyleNum="dsNormal" color="#00A000" selColor="#FF40FF"  bold="0" italic="0"/>
      <itemData name="Structure" defStyleNum="dsNormal" color="#F00000" selColor="#80FFD0" bold="0" italic="0"/>
      <itemData name="Keyword Mathmode" defStyleNum="dsNormal" color="#606000" selColor="#FFD0FF" bold="0" italic="0"/>
      <itemData name="Environment" defStyleNum="dsNormal" color="#0000D0" selColor="#FFFF90" bold="0" italic="0"/>
      <itemData name="Verbatim" defStyleNum="dsNormal" color="#a08000" selColor="#80D0FF" bold="0" italic="0"/>
      <itemData name="Region Marker" defStyleNum="dsNormal" color="#0000F0" selColor="#AAAA90" bold="0" italic="1"/>
    </itemDatas>
  </highlighting>
  <general>
    <keywords weakDeliminator="\" wordWrapDeliminator=",{}[]"/>
    <comments>
      <comment name="singleLine" start="%" />
    </comments>
  </general>
</language>
